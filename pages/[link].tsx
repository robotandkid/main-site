import { GetStaticPaths, GetStaticProps } from "next";
import React from "react";
import { Index, IndexContent } from "../src/components/pages/Index";
import { AppThemeProvider } from "../src/styles/global";
import {
  getStaticProps as staticProps,
  getStaticPaths as staticPaths,
} from "../src/ui/TableOfContents/TableOfContents";

export default Index;

export const getStaticProps: GetStaticProps = staticProps(
  <AppThemeProvider>
    <IndexContent />
  </AppThemeProvider>
);

export const getStaticPaths: GetStaticPaths = staticPaths(
  "link",
  <AppThemeProvider>
    <IndexContent />
  </AppThemeProvider>
);
