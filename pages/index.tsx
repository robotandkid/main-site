import { GetStaticProps } from "next";
import { Index, IndexContent } from "../src/components/pages/Index";
import { AppThemeProvider } from "../src/styles/global";
import { getStaticProps as staticProps } from "../src/ui/TableOfContents/TableOfContents";

export default Index;

export const getStaticProps: GetStaticProps = staticProps(
  <AppThemeProvider>
    <IndexContent />
  </AppThemeProvider>
);
