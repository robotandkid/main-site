import { AppProps } from "next/app";
import { AppThemeProvider, GlobalStyle } from "../src/styles/global";

function ThisApp(props: AppProps) {
  const { Component } = props;

  return (
    <>
      <GlobalStyle />
      <AppThemeProvider>
        <Component {...props.pageProps} />
      </AppThemeProvider>
    </>
  );
}

export default ThisApp;
