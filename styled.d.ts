import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    font: {
      headerFont: string;
    };
    color: {
      primaryColor: string;
      backgroundColor: string;
      secondaryColor: string;
      tertiaryColor: string;
    };
    menu: {
      backgroundColor: string;
      textColor: string;
    };
  }
}
