import styled from "styled-components";
import { UpdateTableOfContents } from "../../ui/TableOfContents/TableOfContents";

const Text = styled.div`
  font-family: sans-serif;
  font-size: 2rem;
`;

export function About() {
  return (
    <>
      <Text>Programmatic art</Text>
      <UpdateTableOfContents href="#about" name="about" index={1} />
    </>
  );
}
