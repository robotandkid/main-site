import { List, XCircle } from "phosphor-react";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import styled, { css, keyframes } from "styled-components";
import { AppThemeProps, NormalizedAnchor } from "../../styles/global";
import { glitchKeyframe } from "../../styles/keyframes";
import { InternalLink } from "../../ui/Link/Link";
import { TableOfContentsContext } from "../../ui/TableOfContents/TableOfContents";

const Container = styled.div`
  position: relative;
`;

const rotateAndFadeKeyframe = keyframes`
  from {
    transform: rotate(0) scale(1.5);
    opacity: 1;
  }
  to {
    transform: rotate(360deg) scale(1.5);
    opacity: 0;
  }
`;

const fadeInKeyframe = keyframes`
  from { opacity: 0 }
  to { opacity: 1}
`;

const rotateKeyFrame = keyframes`
  to {
    transform: rotate(-360deg);
    opacity: 0;
  }
`;

const listIconOpeningAnimation = css`
  animation: ${rotateAndFadeKeyframe} 1s linear forwards;
`;

const xIconOpeningAnimation = css`
  animation: ${fadeInKeyframe} 2s linear 0.5s forwards;
`;

const xIconClosingAnimation = css`
  animation: ${rotateKeyFrame} 1s linear forwards;
`;

const MenuButton = styled.button<{ open: boolean }>`
  outline: none;
  border: none;
  position: fixed;
  top: 1rem;
  left: 1rem;
  background: ${(p: AppThemeProps) => p.theme.color.backgroundColor};
  padding: 0.25rem;

  // This is the second icon
  & > svg:last-child {
    position: absolute;
    left: 0.25rem;
  }

  & > svg {
    height: 3rem;
    width: 3rem;
    transition: all 0.5s;
    transform: scale(1);
  }

  & > svg:hover {
    transform: scale(1.5);
  }

  &:focus > svg {
    transform: scale(1.5);
  }

  & > svg:first-child {
    ${(p) => p.open && listIconOpeningAnimation};
  }

  & > svg:last-child {
    opacity: 0;
    // prevent this icon from grabbing hover events
    ${(p) =>
      !p.open &&
      css`
        z-index: -1;
      `}
    ${(p) => (p.open ? xIconOpeningAnimation : xIconClosingAnimation)}
  }
`;

const StyledMenuContents = styled.div<{ open: boolean }>`
  position: fixed;
  top: 5rem;
  width: max(5rem, 25vw);
  left: min(-5rem, -25vw);
  height: 100vh;
  transition: left 2s;
  background: ${(p) => p.theme.menu.backgroundColor};
  color: ${(p) => p.theme.menu.textColor};
  ${(p) =>
    p.open &&
    css`
      left: 0rem;
    `}

  display: flex;
  flex-direction: column;
`;

const glitch = css<{ children: string; pressed: boolean }>`
  ${(p) =>
    p.pressed &&
    css`
      color: ${(p) => p.theme.color.secondaryColor};
    `};

  :before,
  :after {
    display: block;
    content: "${(p) => p.children}";
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    opacity: 0.8;
  }

  :after {
    color: ${(p) => p.theme.color.secondaryColor};
    z-index: -2;
  }

  :before {
    color: ${(p) => p.theme.color.tertiaryColor};
    z-index: -1;
  }

  :hover,
  :focus {
    &:before {
      animation: ${glitchKeyframe} 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94)
        both infinite;
    }
    &:after {
      animation: ${glitchKeyframe} 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94)
        reverse both infinite;
    }
  }
`;

const MenuItem = styled(NormalizedAnchor)`
  font-family: ${(p) => p.theme.font.headerFont};
  font-size: 2rem;
  margin-left: 2rem;
  margin-top: 2rem;
  outline: none;
  position: relative;

  ${glitch}
`;

function MenuContents(props: { open: boolean; closeMenu: () => void }) {
  const { items } = useContext(TableOfContentsContext);
  const elem = useRef<HTMLDivElement | null>(null);
  const [pressedHref, setPressedHref] = useState<string>();

  useEffect(
    function clearOutSelectionOnClose() {
      let unsub: number;

      if (!props.open) {
        unsub = setTimeout(() => setPressedHref(undefined), 300);
      }

      return () => {
        clearTimeout(unsub);
      };
    },
    [props.open]
  );

  const contentsAreVisible = !!(
    elem.current?.offsetWidth ||
    elem.current?.offsetHeight ||
    elem.current?.getClientRects().length
  );

  return (
    <StyledMenuContents ref={elem} open={props.open}>
      {contentsAreVisible &&
        items.map((item) => {
          return (
            <InternalLink key={item.name} href={item.href}>
              <MenuItem
                pressed={pressedHref === item.href}
                onClick={() => {
                  setPressedHref(item.href);
                  props.closeMenu();
                }}
                onKeyDown={(e) => {
                  if (e.key === " " || e.key === "Enter") {
                    setPressedHref(item.href);
                    props.closeMenu();
                  }
                }}
              >
                {item.name}
              </MenuItem>
            </InternalLink>
          );
        })}
    </StyledMenuContents>
  );
}

export function Menu() {
  const [open, setOpen] = useState(false);
  const prevOpenTime = useRef(0);

  const toggle = useCallback(() => {
    const now = Date.now();

    if (now - prevOpenTime.current > 400) {
      prevOpenTime.current = now;
      setOpen((_open) => !_open);
    }
  }, []);

  useEffect(
    function closeOnBlur() {
      function close() {
        toggle();
      }

      if (open) {
        window.addEventListener("click", close);
      }

      return () => {
        window.removeEventListener("click", close);
      };
    },
    [open, toggle]
  );

  return (
    <Container>
      <MenuButton
        tabIndex={0}
        onClick={toggle}
        open={open}
        aria-label="menu"
        aria-expanded={open}
      >
        <List weight="bold" />
        <XCircle weight="bold" />
      </MenuButton>
      <MenuContents open={open} closeMenu={toggle}></MenuContents>
    </Container>
  );
}
