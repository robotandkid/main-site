import { useEffect, useLayoutEffect, useRef } from "react";
import styled, { css } from "styled-components";
import { AppThemeProps } from "../../styles/global";
import { LinkTarget } from "../../ui/Link/Link";
import { UpdateTableOfContents } from "../../ui/TableOfContents/TableOfContents";
import { useDetectScrolling } from "../../utils/useDetectScrolling";
import { globalWindow } from "../../utils/window";

const titleShrunkSize = "2rem";
const titleFontSize = "6rem";

const Container = styled.div<{ shrink: boolean }>`
  width: 100vw;
  height: 100vh;
  margin: 0;
  padding: 0;
  transition: all 1s;
`;

const shrunkTitle = css`
  top: 1rem;
  right: 2rem;
  font-size: ${titleShrunkSize};
  min-width: auto;
`;

const Title = styled.h1<{ shrink: boolean }>`
  position: fixed;
  font-family: ${(p: AppThemeProps) => p.theme.font.headerFont};
  top: calc(50vh - 2 * ${titleFontSize});
  right: calc(50% - max(50vw, 30rem) / 2);
  font-size: ${titleFontSize};
  min-width: max(50vw, 30rem);
  transition: all 1s;
  line-height: 1.5;
  text-align: center;
  ${(p) => p.shrink && shrunkTitle}
`;

const href = "#title";
const name = "home";

export function LandingHero() {
  const ref = useRef<HTMLDivElement | null>(null);
  const shrink = useDetectScrolling({
    deltaY: (globalWindow()?.innerHeight || 0) * 0.25,
  });

  return (
    <Container ref={ref} shrink={shrink}>
      <LinkTarget id={href}>
        <Title shrink={shrink}>robot and kid</Title>
      </LinkTarget>
      <UpdateTableOfContents index={0} href={href} name={name} />
    </Container>
  );
}
