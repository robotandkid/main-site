import NextLink from "next/link";
import { useRouter } from "next/router";
import React, { useCallback, useEffect, useRef } from "react";

function _target(href: string | undefined) {
  return href?.charAt(0) === "#" ? href?.substring(1) : undefined;
}

interface InternalLinkWrapper {
  tabIndex?: number;
  href: string;
  onKeyUp: (e: React.KeyboardEvent<HTMLAnchorElement>) => void;
  children?: React.ReactNode;
}

export function InternalLink(props: {
  href: string;
  children: React.ReactElement<InternalLinkWrapper>;
}) {
  const target = _target(props.href);
  const { push } = useRouter() || {};

  const onKeyUp = useCallback(
    (e: React.KeyboardEvent<HTMLAnchorElement>) => {
      if (e.key === " " || e.key === "Enter") {
        push({ pathname: "/", query: { link: target } }, undefined, {
          shallow: true,
        });
        e.preventDefault();
      }
    },
    [push, target]
  );

  return (
    <NextLink href={`/?link=${target}`} shallow>
      {React.cloneElement(props.children, {
        tabIndex: 0,
        onKeyUp,
        href: props.href,
      })}
    </NextLink>
  );
}

interface LinkTargetWrapper {
  ref?: React.MutableRefObject<any>;
  id?: string;
  tabIndex?: number;
  children?: React.ReactNode;
}

export const LinkTarget = (props: {
  id: string;
  children: React.ReactElement<LinkTargetWrapper>;
}) => {
  const { query = {} } = useRouter() || {};
  const ref = useRef<HTMLDivElement | null>();

  useEffect(
    function scrollToTargetOnQueryParamChange() {
      let unsubscribe: number | undefined;

      if (query.link === props.id) {
        window.scrollTo({
          top: ref.current?.getBoundingClientRect().top,
          left: 0,
          behavior: "smooth",
        });

        unsubscribe = setTimeout(() => {
          ref.current?.focus();
        }, 1000);

        return () => {
          unsubscribe && clearTimeout(unsubscribe);
        };
      }
    },
    [props.id, query.link]
  );

  return (
    <>
      {React.cloneElement(props.children, { ref, tabIndex: 0, id: props.id })}
    </>
  );
};
