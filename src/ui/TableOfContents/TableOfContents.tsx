import React, { useContext, useMemo, useRef, useState } from "react";
import { renderToString } from "react-dom/server";
import { GetStaticPaths, GetStaticProps } from "next";

export interface TableOfContentsItem {
  /**
   * Must be globally unique
   */
  href: string;
  name: string;
  index: number;
}

interface TableOfContentsInterface {
  items: ReadonlyArray<TableOfContentsItem>;
}

interface UpdateTableOfContentsInterface {
  setItem(item: TableOfContentsItem): void;
}

export const TableOfContentsContext = React.createContext<
  TableOfContentsInterface
>({
  items: [],
});

const UpdateTableOfContentsContext = React.createContext<
  UpdateTableOfContentsInterface
>({
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setItem() {},
});

/**
 * Supports SSR with initial data population using react-dom/renderToString.
 *
 * @param props
 */
export const TableOfContentsProvider = (props: {
  initialItems?: TableOfContentsItem[];
  setItemsForSSR?: (items: TableOfContentsItem[]) => void;
  children?: React.ReactNode;
}) => {
  const items = useRef(props.initialItems || []);
  const [count, setCount] = useState(0);

  const { setItemsForSSR } = props;

  const update = useMemo(
    () => ({
      setItem(item: TableOfContentsItem) {
        if (!items.current.find((x) => x.href === item.href)) {
          items.current.push(item);
          items.current.sort((a, b) => a.index - b.index);
          setCount((count) => count + 1);
          setItemsForSSR?.(items.current);
        }
      },
    }),
    [setItemsForSSR]
  );

  const context = useMemo(() => ({ items: items.current, count }), [count]);

  return (
    <UpdateTableOfContentsContext.Provider value={update}>
      <TableOfContentsContext.Provider value={context}>
        {props.children}
      </TableOfContentsContext.Provider>
    </UpdateTableOfContentsContext.Provider>
  );
};

type UpdateTableOfContentsDuringInitialRenderProps = TableOfContentsItem & {
  setItem: (item: TableOfContentsItem) => void;
};

/**
 * Since setItem is called in the constructor,
 * this can be used by renderToString to get the initial values.
 */
class UpdateTableOfContentsDuringInitialRender extends React.Component<
  UpdateTableOfContentsDuringInitialRenderProps,
  unknown
> {
  constructor(props: UpdateTableOfContentsDuringInitialRenderProps) {
    super(props);

    props.setItem({ href: props.href, name: props.name, index: props.index });
  }

  render() {
    return null;
  }
}

/**
 * Usage: use this as a child component when the parent needs to set the TOC.
 *
 * Exists so that a component can set the TOC without triggering
 * an unnecessary render on itself.
 */
export function UpdateTableOfContents(props: TableOfContentsItem) {
  const { setItem } = useContext(UpdateTableOfContentsContext);

  return (
    <UpdateTableOfContentsDuringInitialRender
      href={props.href}
      name={props.name}
      index={props.index}
      setItem={setItem}
    />
  );
}

export const getStaticProps = (
  Component: JSX.Element
): GetStaticProps => async () => {
  let initialTableOfContents: TableOfContentsItem[] = [];

  const getItems = (items: TableOfContentsItem[]) => {
    initialTableOfContents = [...items];
  };

  const app = () => (
    <TableOfContentsProvider setItemsForSSR={getItems}>
      {Component}
    </TableOfContentsProvider>
  );

  renderToString(app());

  return {
    props: {
      initialTableOfContents,
    },
  };
};

export const getStaticPaths = (
  slugName: string,
  Component: JSX.Element
): GetStaticPaths => async () => {
  let initialTableOfContents: TableOfContentsItem[] = [];

  const getItems = (items: TableOfContentsItem[]) => {
    initialTableOfContents = [...items];
  };

  const app = () => (
    <TableOfContentsProvider setItemsForSSR={getItems}>
      {Component}
    </TableOfContentsProvider>
  );

  renderToString(app());

  return {
    paths: initialTableOfContents.map((item) => ({
      params: { [slugName]: item.href.substring(1) },
    })),
    fallback: false,
  };
};
