import { keyframes } from "styled-components";

export const shakeKeyframe = keyframes`
  10%, 90% {
    transform: translateY(-0.1rem);
  }
  20%, 80% {
    transform: translateY(0.15rem);
  }
  30%, 50%, 70% {
    transform: translateY(-0.35rem);
  }

  40%, 60% {
    transform: translateY(0.35rem);
  }
`;

export const glitchKeyframe = keyframes`
  0% {
    transform: translate(0);
  }
  20% {
    transform: translate(-5px, 5px);
  }
  40% {
    transform: translate(-5px, -5px);
  }
  60% {
    transform: translate(5px, 5px);
  }
  80% {
    transform: translate(5px, -5px);
  }
  to {
    transform: translate(0);
  }
`;
