import styled, {
  createGlobalStyle,
  DefaultTheme,
  ThemeProvider,
} from "styled-components";

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'mrpixel';
    src: url('/fonts/mrpixel.otf');
  }

  body {
    font-size: 62.5%;
    margin: 0;
    padding: 0;
  }
`;

export const NormalizedAnchor = styled.a`
  text-decoration: none;
  color: inherit;
  cursor: pointer;

  &:visited {
    color: inherit;
  }
`;

const theme: DefaultTheme = {
  font: {
    headerFont: "mrpixel",
  },
  color: {
    primaryColor: "black",
    backgroundColor: "white",
    secondaryColor: "#FF00B9",
    tertiaryColor: "#00FEFF",
  },
  menu: {
    backgroundColor: "black",
    textColor: "white",
  },
};

export interface AppThemeProps {
  theme: DefaultTheme;
}

export function AppThemeProvider(props: { children?: React.ReactNode }) {
  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
}
