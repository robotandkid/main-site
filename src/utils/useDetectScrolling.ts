import { useEffect, useRef, useState } from "react";

export function useDetectScrolling({ deltaY }: { deltaY?: number }) {
  const scroll = useRef({ lastKnownScrollY: 0, ticking: false });
  const [scrolling, setScrolling] = useState(false);

  useEffect(() => {
    function trackScrollY() {
      scroll.current.lastKnownScrollY = window.pageYOffset;
      requestTick();
    }

    function requestTick() {
      if (!scroll.current.ticking) {
        requestAnimationFrame(update);
      }

      scroll.current.ticking = true;
    }

    function update() {
      scroll.current.ticking = false;

      const cropValue = deltaY ?? window.innerHeight;

      if (!scrolling && scroll.current.lastKnownScrollY >= cropValue) {
        setScrolling(true);
      } else if (scrolling && scroll.current.lastKnownScrollY < cropValue) {
        setScrolling(false);
      }
    }

    window.addEventListener("scroll", trackScrollY);

    return () => window.removeEventListener("scroll", trackScrollY);
  }, [deltaY, scrolling]);

  return scrolling;
}
