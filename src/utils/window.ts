export function globalWindow(): typeof window | undefined {
  return typeof window !== "undefined" ? window : ({} as typeof window);
}
